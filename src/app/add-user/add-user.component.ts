import { Component, OnInit } from '@angular/core';

import { User} from '../model/user';
import {UserService} from '../service/user.service';

import { Location } from '@angular/common';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent implements OnInit {
  user = new User();
  submitted = false;
 
  constructor(
    private userService: UserService,
    private location: Location
  ) { }
  ngOnInit() {
  }

  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }
 
 addUser() {
   this.submitted = true;
   this.save();
 }
 
  goBack(): void {
    this.location.back();
  }
 
  private save(): void {
    this.userService.addUser(this.user)
        .subscribe();
  }

}
