import { Component, OnInit } from '@angular/core';
import { User} from '../model/user';
 import {UserService} from '../service/user.service';
 import { Location } from '@angular/common';
 
import { ActivatedRoute, Params } from '@angular/router';
 
@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

user = new User();
 submitted = false;
 message:string;
 constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
 ) { } 

 ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id).subscribe(user => this.user= user);
 }
  

}
