import { Component, OnInit } from '@angular/core';
import { ManageUserComponent } from './manage-user/manage-user.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor() { }

    ngOnInit() {
  }

}
