import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListUserComponent} from '../list-user/list-user.component';
import {ListQuizComponent} from '../list-quiz/list-quiz.component';
import {ManageQuizComponent} from '../manage-quiz/manage-quiz.component';
import {HomeComponent} from '../home/home.component';
import {ManageUserComponent} from '../manage-user/manage-user.component';
import {PlayQuizComponent} from '../play-quiz/play-quiz.component';

const routes: Routes = [

  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'users', component: ListUserComponent },
  { path: 'users/:id', component: ManageUserComponent },
  { path: 'users/add', component: ManageUserComponent },
  { path: 'quizes/edit', component: ManageQuizComponent },
  { path: 'quizes/add', component: ManageQuizComponent },
  { path: 'quizes/:id', component: ManageQuizComponent },
  { path: 'quizes', component: ListQuizComponent },
  { path: 'play', component: PlayQuizComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}

