import {Question} from './question';

export class Answer {
  id: number;
  text: string;
  order: number;
  createdDate: string;
  isCorrect: boolean;
  //question: Question;
}
