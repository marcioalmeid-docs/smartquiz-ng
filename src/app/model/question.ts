import { Quiz } from './quiz';
import {Answer} from './answer';
import { Option } from './option';

export class Question {
  id: number;
  text: string;
  order: number;
  answers: Answer[];
  correctAnswer: Answer;
  createdDate: string;
  questionTypeId: number; // TODO create this attribute in java
  options: Option[]; // used to list answers in the play module
  answered: boolean; // TODO create this attribute in java
  quiz: Quiz;

      constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.text = data.text;
        this.questionTypeId = data.questionTypeId;
        this.options = [];
        this.quiz = data.quiz;
        this.correctAnswer = data.correctAnswer;
   
//        for (let i = 0; i < data.answers.length; i++) {
//          this.options.push(new Option(data.answers[i]));
//        }
    }

}
