import {User} from '../model/user';
import {Question} from '../model/question';
import { QuizConfig } from './quiz-config';


export class Quiz {
  id: number;
  name: string;
  description: string;
  createDate: string;
  isPublished: boolean;
  createdBy: User;
  questions: Question[];
  config: QuizConfig;

      constructor(data: any) {
        if (data) {
            this.id = data.id;
            this.name = data.text;
            this.description = data.description;
            this.config = new QuizConfig(data.config);
            this.questions = [];
            data.questions.forEach(q => {
                this.questions.push(new Question(q));
            });
        }
    }
}
