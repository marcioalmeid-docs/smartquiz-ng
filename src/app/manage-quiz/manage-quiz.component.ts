import {Question, Option} from '../model';
import {Answer} from '../model/answer';
import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Quiz} from '../model/quiz';
import {QuizConfig} from '../model/quiz-config';
import {QuizService} from '../service/quiz.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-manage-quiz',
  templateUrl: './manage-quiz.component.html',
  styleUrls: ['./manage-quiz.component.css']
})

export class ManageQuizComponent implements OnInit {

  private edit: Boolean = false;
  private title = 'View Quiz';
  private quiz: Quiz;
  private questions: Question[];
  private question: Question;
  private answers: Answer[];
  private idTempQuiz = 0;
  private idTempQuestion = 0;
  private idTempAnswer = 0;
  private checkboxValue: boolean;
  private answer: Answer;
  private correctAnswer: Answer;

  config: QuizConfig = {
    'allowBack': true,
    'allowReview': true,
    'autoMove': false,  // if true, it will move to next question automatically when answered.
    'duration': 60,  // indicates the time (in secs) in which quiz needs to be completed. 0 means unlimited.
    'pageSize': 1,
    'requiredAll': false,  // indicates if you must answer all the questions before submitting.
    'richText': false,
    'shuffleQuestions': false,
    'shuffleOptions': false,
    'showClock': false,
    'showPager': true
  };

  constructor(private quizService: QuizService,
    private route: ActivatedRoute, private location: Location) {}

  ngOnInit(): void {

     this.quiz = new Quiz(null);
      this.question = new Question(null);
      this.questions = [];
      this.answers = [];
      this.answer = new Answer();
      this.edit = false;
      this.correctAnswer = new Answer();

    const id = +this.route.snapshot.paramMap.get('id');
    if (!isNaN(id)) {
      console.log('Get Quiz id' + id);
      this.quizService.getQuiz(id).subscribe(
        quiz => {this.quiz = quiz; this.questions = this.quiz.questions; },
        error => {console.log('Error to find Quiz ID'); },
        () => {console.log('Get Quiz ID completed sucessfully'); });
    } else {
      console.log('New QUIZ!!');

    }
  }

  saveQuiz() {

    this.quiz.questions = this.questions;
    console.log('Q Correct answer: ' + this.quiz.questions[this.question.id].correctAnswer.text);
    const test = JSON.stringify(this.quiz);
    console.log('Test Json: ' + test);

    this.quizService.addQuiz(this.quiz).subscribe(
      value => {console.log('Quiz created sucessfully', value); this.edit = false; this.title = 'View Quiz'; },
      error => {console.log('Error to create Quiz' + error); },
      () => {console.log('Save Quiz completed sucessfully'); }
    );

    this.idTempQuiz++;
  }

  addQuestion(newQuestion: Question) {
    this.answers = new Array<Answer>();
    newQuestion = new Question(null);
    const name = this.question.text;
    this.idTempQuestion = this.questions.length;
    newQuestion.id = this.idTempQuestion;
    // newQuestion.quiz = this.quiz;//NOT HAVE ID YET
    newQuestion.text = name;
    newQuestion.answers = this.answers;
    newQuestion.correctAnswer = this.answers[0]; // Need to be object
    newQuestion.order = 0; // NEED TO BE FILLED
    newQuestion.createdDate = '2018-08-07';

    this.questions.push(newQuestion);
    console.log('Test question saved in questions[]: ' + this.questions[this.idTempQuestion].text);
    this.question = newQuestion;
  }

  addAnswer(newAnswer: Answer) {
    newAnswer = new Answer();
    this.idTempAnswer = this.answers.length;
    const name = this.answer.text;
    newAnswer.id = this.idTempAnswer;
    newAnswer.text = name;
    newAnswer.createdDate = '2018-08-07';
    newAnswer.order = 0; // NEED TO BE FILLED
    //  newAnswer.isCorrect=false;//REMOVE CORRECT answers from QUESTION

    // newAnswer.question = this.questions[this.idTempQuestion];
    // checkCorrectAnswer ADD answer in the question
    this.answers.push(newAnswer);
    console.log('Checked as correct ID: ' + this.questions[this.idTempQuestion]);
  }

  // checkCorrectAnswer ADD answer in the question
  checkCorrectAnswer(answer: any) {
    this.answer = answer;
    if (this.checkboxValue) {

     this.correctAnswer = answer;
      this.question.correctAnswer = this.correctAnswer;
     this.quiz.questions[this.question.id] = this.question;
     console.log(' CORRECT ANSWER' + this.quiz.questions[this.question.id].correctAnswer.text);

    } else {
      this.question.correctAnswer = null;
    }

  }

  editOnOff() {
    this.edit = !this.edit;
    if (this.edit) {
      this.title = 'Edit Quiz';
    } else {
      this.title = 'View Quiz';
    }
  }

  goBack() {
    this.location.back();
  }

  deleteQuiz() {
    console.log('Delete quiz ' + this.quiz.id);
    this.quizService.deleteQuiz(this.quiz.id).subscribe(
      result => console.log(result),
      err => console.error(err),
      () => console.log('Complete!!')
    );
  }
}
