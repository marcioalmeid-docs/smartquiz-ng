import {Component, OnInit} from '@angular/core';
import {Quiz} from '../model/quiz';
import {QuizService} from '../service/quiz.service';

@Component({
  selector: 'app-list-quiz',
  templateUrl: './list-quiz.component.html',
  styleUrls: ['./list-quiz.component.css']
})

export class ListQuizComponent implements OnInit {

  quizes: Quiz[];

  constructor(private quizService: QuizService) {}

  ngOnInit() {
    this.getQuizes();
  }

  getQuizes() {
    return this.quizService.getQuizes()
      .subscribe(
      quizes => {
        console.log(quizes);
        this.quizes = quizes;
      }
      );
  }

}
