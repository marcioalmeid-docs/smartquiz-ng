import {User} from '../model/user';
import {UserService} from '../service/user.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent implements OnInit {
  edit = false;
  user = new User();
  submitted = false;
  message: string;
  title = 'View User';

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (!isNaN(id)) {
      console.log('Get User id' + id);
      this.userService.getUser(id).subscribe(user => this.user = user);
    } else {
        console.log('New User!!');
    }
  }

  addUser() {
    this.submitted = true;
    this.userService.addUser(this.user).subscribe(
      value => {console.log('User saved succesfully', value); },
      error => {console.log('Error to save user'); },
      () => {console.log('Save user completed sucessfully'); }
    );
  }

  editUser() {
    this.submitted = true;
    this.userService.addUser(this.user).subscribe(
      value => {console.log('User edited succesfully', value); },
      error => {console.log('Error to eidt user'); },
      () => {console.log('Edit user completed sucessfully'); }
    );
  }

  deleteUser() {
    this.userService.deleteUser(this.user.id).subscribe(
      value => {console.log('User deleted succesfully', value); },
      error => {console.log('Error to delete user'); },
      () => {console.log('Delete user completed sucessfully'); }
    );
  }

  editOnOff() {
     this.edit = !this.edit;
    if (this.edit) {
        this.title = 'Edit User';
    } else {
      this.title = 'View User';
    }
  }

    goBack() {
    this.location.back();
  }
}
