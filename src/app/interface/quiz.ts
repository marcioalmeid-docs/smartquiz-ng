export interface Quiz {
  id: number;
  name: string;
  description: string;
  createDate: string;
  isPublished: boolean;
  createdBy: User;
}

export interface User {
  id: number;
  email: string;
  username: string;
  createdDate: string;
}
