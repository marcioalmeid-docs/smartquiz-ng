import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {AppComponent} from './app.component';
import {ListUserComponent} from './list-user/list-user.component';
import {ListQuizComponent} from './list-quiz/list-quiz.component';
import {ManageQuizComponent} from './manage-quiz/manage-quiz.component';
import {PlayQuizComponent} from './play-quiz/play-quiz.component';
import {HomeComponent } from './home/home.component';
import {UiModule } from './ui/ui.module';
import {ManageUserComponent } from './manage-user/manage-user.component';

@NgModule({
  declarations: [
    AppComponent,
    ListUserComponent,
    ListQuizComponent,
    ManageQuizComponent,
    PlayQuizComponent,
    HomeComponent,
    ManageUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    UiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
