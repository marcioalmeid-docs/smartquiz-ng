import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Quiz} from '../model/quiz';

const httpOptions = {headers: new HttpHeaders({'Content-type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private quizUrl = 'http://localhost:8080/api/quizes/';  // URL to web api

  constructor(private http: HttpClient) {}

  getQuiz(id: number): Observable<Quiz> {
    return this.http.get<Quiz>(this.quizUrl + 'edit/' + `${id}`);
  }

  getQuizes(): Observable<Quiz[]> {

    return this.http.get<Quiz[]>(this.quizUrl);
  }

  getQuizByName(name: string) {
    return this.http.get(this.quizUrl + 'find/' + `${name}`);
  }

  addQuiz(quiz: Quiz): Observable<Quiz> {
    console.log('Test object quiz exists? >> ' + quiz.name);
    //console.log('Questions in Quiz? >> ' + quiz.questions.length);
    //console.log('addAnswers in questions >> ' + quiz.questions[0].answers.length);
    return this.http.post<Quiz>(this.quizUrl + 'save/', quiz, httpOptions);
  }
  
  deleteQuiz(id: number): Observable<Quiz> {
    return this.http.delete<Quiz>(this.quizUrl + 'delete/' + `${id}`);
  }
  
}
