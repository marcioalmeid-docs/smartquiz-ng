import {Component, OnInit} from '@angular/core';
import {Quiz} from '../interface/quiz';
import {QuizService} from '../service/quiz.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  quizes: Quiz[];
  constructor(private quizService: QuizService) {}

  ngOnInit() {
    this.getQuizes();
  }

  getQuizes() {
    return this.quizService.getQuizes().subscribe(
      quizes => {
        console.log(quizes);
        this.quizes = quizes;
      }
    )
  }

}
