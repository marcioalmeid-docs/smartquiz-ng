import { Component, OnInit } from '@angular/core';
import {QuizService} from '../service/quiz.service';
import {Quiz} from '../model/quiz';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-edit-quiz',
  templateUrl: './edit-quiz.component.html',
  styleUrls: ['./edit-quiz.component.css']
})
  
export class EditQuizComponent implements OnInit {
  //quiz = new Quiz();  
  quiz : Quiz = {id:0, name:' ', description:'', createdDate:'', createdBy: { id:0, email:'', username:'', createdDate: '' }};
  

  submitted = false;
  
  constructor( private quizService:QuizService , private route : ActivatedRoute) { }

  ngOnInit() {
       const id = +this.route.snapshot.paramMap.get('id');
    this.quizService.getQuiz(id).subscribe(
      value => {
        console.log('[POST] load Quiz successfully', value);
        this.quiz = value;
      }, error => {
        console.log('FAIL to load Quiz!');
      },
      () => {
        console.log('POST Quiz - now completed.');
      });
  }
 
  
  onSubmit() {
    
    this.quizService.addQuiz(this.quiz).subscribe(
      value => {
        console.log('[POST] create Quiz successfully', value);
      }, error => {
        console.log('FAIL to create Quiz!');
      },
      () => {
        console.log('POST Quiz - now completed.');
      });
  }
  
}
